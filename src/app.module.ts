import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TaskModule } from './task/task.module';
import { CommentModule } from './comment/comment.module';
import { RunModule } from './run/run.module';

@Module({
  imports: [UserModule, TaskModule, CommentModule, RunModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
