import { Injectable } from "@nestjs/common";    
import { Comment } from "./comment.interface";
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
@Injectable()
export class CommentRepository{
    private comments: Comment[] = [
        {
            id:1,
            text:'Коментарий к задаче',
            user_id:2,
            task_id:1,
            dt:'20.12.2000',
        },
        {
            id:2,
            text:'Коментарий 2 к задаче',
            user_id:1,
            task_id:4,
            dt:'21.10.2002',
        },
        {
            id:3,
            text:'Коментарий 3 к задаче',
            user_id:4,
            task_id:2,
            dt:'02.02.2000',
        }
    ];

    findAll():Comment[]{
        return this.comments;
    }
    findOne(id:number): Comment | null {
        return this.comments.find((t: Comment):boolean => t.id === id) ?? null;
    }

    create(commentDto: CreateCommentDto){
        const comment: Comment = {
            id: this.comments.length + 1,
            text: commentDto.text,
            user_id: commentDto.user_id,
            task_id: commentDto.task_id,
            dt: commentDto.dt,
        };
        
        this.comments.push(comment);
        return comment;        
    }

    update(id: number, updateCommentDto: UpdateCommentDto) {
      
        const idx:number =this.comments.findIndex((t: Comment):boolean => t.id === id);
        this.comments[idx]={
            ...this.comments[idx],
            ...updateCommentDto,
        };
        return this.comments[idx];
    }
    remove(id: number){
        this.comments = this.comments.filter(t => t.id !==id);
    }
}