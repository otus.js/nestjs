import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { CommentRepository } from './comment.repository';
import { Comment } from './comment.interface';


@Injectable()
export class CommentService {
  constructor(private readonly repository: CommentRepository){}

  create(createCommentDto: CreateCommentDto) {
   return this.create(createCommentDto);
  }

  findAll() {
    return this.repository.findAll();
  }

  findOne(id: number) {
    const comment : Comment = this.repository.findOne(id);
    if (!comment){
      throw new NotFoundException(`Comment wjth id ${id} not found`);
    }
    return comment;
  }

  update(id: number, updateCommentDto: UpdateCommentDto) {
    const comment : Comment = this.repository.findOne(id);
    if (!comment){
      throw new NotFoundException(`Comment wjth id ${id} not found`);
    }
    return this.repository.update(id, updateCommentDto);
  }

  remove(id: number) {
    return this.repository.remove(id);
  }
}
