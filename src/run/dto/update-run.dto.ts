import { PartialType } from '@nestjs/mapped-types';
import { CreateRunDto } from './create-run.dto';

export class UpdateRunDto extends PartialType(CreateRunDto) {
    user_id:number;
    task_id:number;
    user_code:string;
    language:string;
    dt:string;
    hashcode:string;
}
