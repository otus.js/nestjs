import { Module } from '@nestjs/common';
import { RunService } from './run.service';
import { RunController } from './run.controller';
import { RunRepository } from './run.repository';

@Module({
  controllers: [RunController],
  providers: [RunService, RunRepository],
})
export class RunModule {}
