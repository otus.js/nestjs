import { Injectable } from "@nestjs/common";    
import { Run } from "./run.interface";
import { CreateRunDto } from './dto/create-run.dto';
import { UpdateRunDto } from './dto/update-run.dto';
@Injectable()
export class RunRepository{
    private runs:Run[] = [
        {
            id:1,
            user_id:1,
            task_id:1,
            user_code:'решение задачи',
            language:'JS',
            dt:'01.01.2000',
            hashcode:'js'
        },
        {
            id:2,
            user_id:3,
            task_id:4,
            user_code:'решение задачи',
            language:'C++',
            dt:'01.10.2001',
            hashcode:'C++,Массивы'
        },
        {
            id:3,
            user_id:1,
            task_id:4,
            user_code:'решение задачи',
            language:'JS',
            dt:'01.10.2001',
            hashcode:'JS,Массивы'
        }
    ];

    findAll():Run[]{
        return this.runs;
    }
    findOne(id:number): Run | null {
        return this.runs.find((t: Run):boolean => t.id === id) ?? null;
    }

    create(runsDto: CreateRunDto){
        const run: Run = {
            id: this.runs.length + 1,
            user_id: runsDto.user_id,
            task_id: runsDto.task_id,
            user_code: runsDto.user_code,
            language: runsDto.language,
            dt: runsDto.dt,
            hashcode: runsDto.hashcode,
        };
        
        this.runs.push(run);
        return run;        
    }

    update(id: number, updateRunDto: UpdateRunDto) {
      
        const idx: number =this.runs.findIndex((t: Run):boolean => t.id === id);
        this.runs[idx]={
            ...this.runs[idx],
            ...updateRunDto,
        };
        return this.runs[idx];
    }
    remove(id: number){
        this.runs = this.runs.filter(t => t.id !==id);
    }
}