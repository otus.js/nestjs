import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRunDto } from './dto/create-run.dto';
import { UpdateRunDto } from './dto/update-run.dto';
import { RunRepository } from './run.repository';
import { Run } from './run.interface';

@Injectable()
export class RunService {
  constructor(private readonly repository: RunRepository){}

  create(createRunDto: CreateRunDto) {
    return this.create(createRunDto);
  }

  findAll() {
    return this.repository.findAll();
  }

  findOne(id: number) {
    const run : Run = this.repository.findOne(id);
    if (!run){
      throw new NotFoundException(`Run wjth id ${id} not found`);
    }
    return run;
  }

  update(id: number, updateRunDto: UpdateRunDto) {
    const run : Run = this.repository.findOne(id);
    if (!run){
      throw new NotFoundException(`Run wjth id ${id} not found`);
    }
    return this.repository.update(id, updateRunDto);
  }

  remove(id: number) {
    return this.repository.remove(id);
  }
}
