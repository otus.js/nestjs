import { Injectable } from "@nestjs/common";
import { Task } from "./task.interface";
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TaskRepository{
    private tasks:Task[] = [
        {
            id:1,
            title:'Условия',
            code:'задача №1 по теме условие',
            correct_code:'решение задачи №1 по теме условие',
        },
        {
            id:2,
            title:'Циклы',
            code:'задача №1 по теме Циклы',
            correct_code:'решение задачи №1 по теме Циклы',
        },
        {
            id:3,
            title:'Массивы',
            code:'задача №1 по теме Массивы',
            correct_code:'решение задачи №1 по теме Массивы',
        },
        {
            id:4,
            title:'Массивы',
            code:'задача №2 по теме Массивы',
            correct_code:'решение задачи №2 по теме Массивы',
        }
    ];

    findAll():Task[]{
        return this.tasks;
    }
    findOne(id:number): Task | null {
        return this.tasks.find((t: Task):boolean => t.id === id) ?? null;
    }

    create(taskDto: CreateTaskDto){
        const task: Task = {
            id: this.tasks.length + 1,
            title: taskDto.title,
            code: taskDto.code,
            correct_code: taskDto.correct_code,
        };
        
        this.tasks.push(task);
        return task;        
    }

    update(id: number, updateUserDto: UpdateTaskDto) {
      
        const idx:number =this.tasks.findIndex((t: Task):boolean => t.id === id);
        this.tasks[idx]={
            ...this.tasks[idx],
            ...updateUserDto,
        };
        return this.tasks[idx];
    }
    remove(id: number){
        this.tasks=this.tasks.filter(t => t.id !==id);
    }
}