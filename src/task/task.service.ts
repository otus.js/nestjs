import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TaskRepository } from './task.repository';
import { Task } from './task.interface';

@Injectable()
export class TaskService {
  constructor(private readonly repository: TaskRepository){}

  create(createTaskDto: CreateTaskDto) {
    return this.create(createTaskDto);
  }

  findAll() {
    return this.repository.findAll();
  }

  findOne(id: number) {
    const task : Task = this.repository.findOne(id);
    if (!task){
      throw new NotFoundException(`Task wjth id ${id} not found`);
    }
    return task;
  }

  update(id: number, updateTaskDto: UpdateTaskDto) {
    const task : Task = this.repository.findOne(id);
    if (!task){
      throw new NotFoundException(`Task wjth id ${id} not found`);
    }
    return this.repository.update(id, updateTaskDto);
  }

  remove(id: number) {
    return this.repository.remove(id);
  }
}
