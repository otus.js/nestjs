import { Injectable } from "@nestjs/common";    
import { User } from "./user.interface";
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserRepository{
    private users: User[] = [
        {
            id:1,
            name:'Иванов Иван Иванович',
            email:'john.c.calhoun@examplepe',
            password:'12345',
        },
        {
            id:2,
            name:'Иванова Иванна Игоревна',
            email:'ivanna_ivanova@examplepe',
            password:'aassdd',
        },
        {
            id:3,
            name:'Петров Игорь Игнатьевич',
            email:'petrov123@mail.ru',
            password:'1qasw23ed',
        }
    ];

    findAll():User[]{
        return this.users;
    }
    
    findById(id:number): User | null {
        return this.users.find((u:User):boolean => u.id === id) ?? null;
    }

    create(userDto: CreateUserDto){
        const user: User = {
            id: this.users.length + 1,
            name: userDto.name,
            email: userDto.email,
            password: userDto.password,
        };
        // const user: User = {
        //     id:10,
        //     name:'Иванов1 Иван Иванович',
        //     email:'john_calhoun@examplepe',
        //     password:'1dfg5',
        // };
        this.users.push(user);
        return user;        
    }

    update(id: number, updateUserDto: UpdateUserDto) {
        // const user = this.findById(id);
        // if(user){
        //     user.name = updateUserDto.name;
        //     user.email = updateUserDto.email;
        //     user.password = updateUserDto.password;
        // }
        // return user;
        const idx:number =this.users.findIndex((u:User):boolean => u.id === id);
        this.users[idx]={
            ...this.users[idx],
        ...updateUserDto,
        };
        return this.users[idx];
    }
    remove(id: number){
        this.users=this.users.filter(u=>u.id !==id);
    }
}