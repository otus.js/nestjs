import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserRepository } from './user.repository';
import { User } from './user.interface';

@Injectable()
export class UserService {
  constructor(private readonly repository: UserRepository){}

  create(createUserDto: CreateUserDto) {
    return this.create(createUserDto);
  }

  findAll() {
    return this.repository.findAll();
  }

  findById(id: number) : User {
    const user : User = this.repository.findById(id);
    if (!user){
      throw new NotFoundException(`User wjth id ${id} not found`);
    }
    return user;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const user : User = this.repository.findById(id);
    if (!user){
      throw new NotFoundException(`User wjth id ${id} not found`);
    }
    return this.repository.update(id, updateUserDto);
  }

  remove(id: number) {
    return this.repository.remove(id);
  }
}
